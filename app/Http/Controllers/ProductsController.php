<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductsController extends Controller
{

    public function index()
    {
        $products = @file_get_contents(storage_path('app/stock.json'));
        $products = ($products) ? collect(json_decode($products, true)) : collect([]);
        return $products;
    }

    public function store()
    {
        $products = @file_get_contents(storage_path('app/stock.json'));
        $products = ($products) ? collect(json_decode($products, true)) : collect([]);

        $product = [
            'name' => request('name'),
            'quantity' => request('quantity'),
            'price' => request('price'),
            'value' => request('quantity') * request('price'),
            'created_at' => now()
        ];
        $products->push($product);
        $jsonEncode = json_encode($products);
        file_put_contents(storage_path('app/stock.json'), $jsonEncode);

        return $products;
    }

    public function update($index)
    {
        $products = @file_get_contents(storage_path('app/stock.json'));
        $products = ($products) ? collect(json_decode($products, true)) : collect([]);
        $product = $products[$index];
        $product['name'] = request('name');
        $product['quantity'] = request('quantity');
        $product['price'] = request('price');
        $product['value'] = request('quantity') * request('price');

        $products[$index] = $product;

        $jsonEncode = json_encode($products);
        file_put_contents(storage_path('app/stock.json'), $jsonEncode);

        return $products;
    }
}
